package com.nlmkit.korshunov_am.tm.exceptions;

/**
 * Исключение не найден проект
 */
public class ProjectNotFoundException extends MessageException {
    /**
     * Конструктор
     * @param message Сообщение об ошибке
     */
    public ProjectNotFoundException(String message) {
        super(message);
    }
}
