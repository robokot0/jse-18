package com.nlmkit.korshunov_am.tm.exceptions;

/**
 * Исключение не найдена задача
 */
public class TaskNotFoundException extends MessageException {
    /**
     * Конструктор
     * @param message Сообщение об ошибке
     */
    public TaskNotFoundException(String message) {
        super(message);
    }
}
