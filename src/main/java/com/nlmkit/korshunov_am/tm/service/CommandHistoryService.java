package com.nlmkit.korshunov_am.tm.service;

import com.nlmkit.korshunov_am.tm.entity.Command;
import com.nlmkit.korshunov_am.tm.repository.CommandHistoryRepository;

import java.util.List;

public class CommandHistoryService {
    private final CommandHistoryRepository сommandHistoryRepository;

    /**
     * Конструктор по умолчанию
     * @param сommandHistoryRepository
     */
    public CommandHistoryService(CommandHistoryRepository сommandHistoryRepository) {
        this.сommandHistoryRepository = сommandHistoryRepository;
    }

    /**
     * Добавляет комманду в историю
     * @param command комманда
     */
    public void AddCommandToHistory(String command) {
        сommandHistoryRepository.AddCommandToHistory(command);
    }

    /**
     * Добавляет параметр комманды в историю
     * @param commandParameter параметр комманды
     * @param parameterValue значение параметра
     */
    public void AddCommandParameterToLastCommand(String commandParameter, String parameterValue) {
        сommandHistoryRepository.AddCommandParameterToLastCommand(commandParameter, parameterValue);
    }

    /**
     * Добавляет результат исполнения комманды
     * @param commandResult результат исполнения комманды
     */
    public void AddCommandResultToLastCommand(String commandResult) {
        сommandHistoryRepository.AddCommandResultToLastCommand(commandResult);
    }

    /**
     * Получить список всех комманд из истории
     * @return список комманд
     */
    public List<Command> findAll() {
        return сommandHistoryRepository.findAll();
    }
}
