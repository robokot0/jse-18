package com.nlmkit.korshunov_am.tm.service;

import com.nlmkit.korshunov_am.tm.entity.Project;
import com.nlmkit.korshunov_am.tm.entity.Task;
import com.nlmkit.korshunov_am.tm.exceptions.ProjectNotFoundException;
import com.nlmkit.korshunov_am.tm.exceptions.TaskNotFoundException;
import com.nlmkit.korshunov_am.tm.exceptions.WrongArgumentException;
import com.nlmkit.korshunov_am.tm.repository.ProjectRepository;
import com.nlmkit.korshunov_am.tm.repository.TaskRepostory;

import java.util.Collections;
import java.util.List;

/**
 * Сервис задачи в проекте
 */
public class ProjectTaskService {
    /**
     * Репозитарий проектов
     */
    private final ProjectRepository projectRepository;
    /**
     * Репозитарий задач
     */
    private final TaskRepostory taskRepostory;

    /**
     * Конструктор
     * @param projectRepository репозитарий проектов
     * @param taskRepostory репозитарий задач
     */
    public ProjectTaskService(ProjectRepository projectRepository, TaskRepostory taskRepostory) {
        this.projectRepository = projectRepository;
        this.taskRepostory = taskRepostory;
    }

    /**
     * Получить список задач проекта
     * @param projectId идентификатор проекта
     * @return список задач
     */
    public List<Task> findAllByProjectId(final Long projectId) throws WrongArgumentException{
        if (projectId == null) throw new WrongArgumentException("Не задано ИД проекта");
        return taskRepostory.findAllByProjectId(projectId);
    }

    /**
     * Получить список задач проекта с учетом пользователя
     * @param projectId идентификатор проекта
     * @param userId ид пользователя
     * @return список задач
     */
    public List<Task> findAllByProjectId(final Long projectId,final Long userId) throws WrongArgumentException {
        if (projectId == null)  throw new WrongArgumentException("Не задано ИД проекта");
        return taskRepostory.findAllByProjectId(projectId,userId);
    }

    /**
     * Удалить задачу из проекта
     * @param projectId идентификатор проекта
     * @param taskId идентификатор задачи
     * @return задача
     */
    public Task removeTaskFromProject(final Long projectId, final Long taskId) throws WrongArgumentException, TaskNotFoundException {
        if (projectId == null)  throw new WrongArgumentException("Не задано ИД проекта");
        if (taskId == null) throw new WrongArgumentException("Не задано ИД задачи");
        final Task task = taskRepostory.findByProjectIdAndId(projectId,taskId);
        if (task == null) throw new TaskNotFoundException("Не найдена задача а проекте для удаления из проекта");
        task.setProjectId(null);
        return task;
    }

    /**
     * Удалить задачу из проекта с учетом задачи и проекта
     * @param projectId идентификатор проекта
     * @param taskId идентификатор задачи
     * @param userId ид пользователя
     * @return задача
     */
    public Task removeTaskFromProject(final Long projectId, final Long taskId,final Long userId) throws WrongArgumentException,TaskNotFoundException{
        if (userId == null) throw new WrongArgumentException("Не задано ид пользователя");
        if (projectId == null) throw new WrongArgumentException("Не задано ИД проекта");
        if (taskId == null) throw new WrongArgumentException("Не задано ИД задачи");
        final Task task = taskRepostory.findByProjectIdAndId(projectId,taskId,userId);
        if (task == null) throw new TaskNotFoundException("Не найдена задача пользователя в проекте");
        task.setProjectId(null);
        return task;
    }

    /**
     * Добавить задачу к проекту
     * @param projectId идентификатор проекта
     * @param taskId идентификатор задачи
     * @return задача
     */
    public Task addTaskToProject(final Long projectId, final Long taskId) throws TaskNotFoundException, ProjectNotFoundException {
        final Project project = projectRepository.findById(projectId);
        if (project == null) throw new ProjectNotFoundException("Не найен проект по ИД");
        final Task task = taskRepostory.findById(taskId);
        if (task == null) throw new TaskNotFoundException("Не найдена задача по ИД");
        task.setProjectId(projectId);
        return task;
    }

    /**
     * Добавить задачу к проекту с учетом пользователя
     * @param projectId идентификатор проекта
     * @param taskId идентификатор задачи
     * @param userId ид пользователя
     * @return задача
     */
    public Task addTaskToProject(final Long projectId, final Long taskId,final Long userId) throws TaskNotFoundException, ProjectNotFoundException {
        final Project project = projectRepository.findById(projectId,userId);
        if (project == null) throw new ProjectNotFoundException("Не найен проект пользователя по ИД");
        final Task task = taskRepostory.findById(taskId,userId);
        if (task == null) throw new TaskNotFoundException("Не найдена задача пользователя по ИД");
        task.setProjectId(projectId);
        return task;
    }

}
