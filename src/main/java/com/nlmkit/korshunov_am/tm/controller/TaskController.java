package com.nlmkit.korshunov_am.tm.controller;

import com.nlmkit.korshunov_am.tm.entity.Task;
import com.nlmkit.korshunov_am.tm.entity.User;
import com.nlmkit.korshunov_am.tm.exceptions.ProjectNotFoundException;
import com.nlmkit.korshunov_am.tm.exceptions.TaskNotFoundException;
import com.nlmkit.korshunov_am.tm.exceptions.WrongArgumentException;
import com.nlmkit.korshunov_am.tm.service.CommandHistoryService;
import com.nlmkit.korshunov_am.tm.service.ProjectTaskService;
import com.nlmkit.korshunov_am.tm.service.TaskService;

import java.util.List;

/**
 * Контроллер задач
 */
public class TaskController extends AbstractController {
    /**
     * Сервис задач
     */
    private final TaskService taskService;
    /**
     * Сервис задач в проекте
     */
    private final ProjectTaskService projectTaskService;
    /**
     * Конструктор
     * @param taskService Сервис задач
     * @param projectTaskService Сервис задач в проекте
     */
    public TaskController(final TaskService taskService,final  ProjectTaskService projectTaskService, final CommandHistoryService commandHistoryService) {
        super(commandHistoryService);
        this.taskService = taskService;
        this.projectTaskService = projectTaskService;
    }
    /**
     * Изменить задачу
     * @param task задача
     * @return 0 выполнено
     */
    public int updateTask(final Task task) throws WrongArgumentException {

        final String name = EnterStringCommandParameter("task name");
        final String description = EnterStringCommandParameter("task description");
        taskService.update(task.getId(),name,description,task.getUserId());
        ShowResult("[OK]");
        return 0;
    }
    /**
     * Изменить задачу по индексу
     * @return 0 выполнено
     */
    public int updateTaskByIndex() throws WrongArgumentException, TaskNotFoundException {
        System.out.println("[UPDATE TASK BY INDEX]");
        if (!this.testAuthUser())return 0;
        final int index = EnterIntegerCommandParameter("task index")-1;
        final Task task = this.getUser().isAdmin() ?
                taskService.findByIndex(index,true):taskService.findByIndex(index,this.getUser().getId(),true);
        updateTask(task);
        return 0;
    }

    /**
     * Удалить задачу по имени
     * @return 0 выполнено
     */
    public int removeTaskByName() throws WrongArgumentException, TaskNotFoundException {
        System.out.println("[REMOVE TASK BY NAME]");
        if (!this.testAuthUser())return 0;
        final String name = EnterStringCommandParameter("task name");
        final Task task = this.getUser().isAdmin() ?
                taskService.removeByName(name):taskService.removeByName(name,this.getUser().getId());
        ShowResult("[OK]");
        return 0;
    }

    /**
     * Удалить задачу по ID
     * @return 0 выполнено
     */
    public int removeTaskByID() throws WrongArgumentException, TaskNotFoundException {
        System.out.println("[REMOVE TASK BY ID]");
        if (!this.testAuthUser())return 0;
        final long id = EnterLongCommandParameter("task ID");
        final Task task = this.getUser().isAdmin() ?
                taskService.removeById(id):taskService.removeById(id,this.getUser().getId());
        ShowResult("[OK]");
        return 0;
    }

    /**
     * Удалить задачу по имени
     * @return 0 выполнено
     */
    public int removeTaskByIndex() throws WrongArgumentException, TaskNotFoundException {
        System.out.println("[REMOVE TASK BY INDEX]");
        if (!this.testAuthUser())return 0;
        final int index = EnterIntegerCommandParameter("task index")-1;
        final Task task = this.getUser().isAdmin() ?
                taskService.removeByIndex(index):taskService.removeByIndex(index,this.getUser().getId());
        ShowResult("[OK]");
        return 0;
    }
    /**
     * Создать задачу
     * @return 0 выполнено
     */
    public int createTask() throws WrongArgumentException {
        System.out.println("[CREATE TASK]");
        if (!this.testAuthUser())return 0;
        final String name = EnterStringCommandParameter("task name");
        final String description = EnterStringCommandParameter("task description");
        taskService.create(name,description,this.getUser().getId());
        ShowResult("[OK]");
        return 0;
    }

    /**
     * Удалить все задачи
     * @return 0 выполнено
     */
    public int clearTask() throws WrongArgumentException {
        System.out.println("[CLEAR TASK]");
        if (!this.testAuthUser())return 0;
        if(this.getUser().isAdmin()) taskService.clear();
        else taskService.clear(this.getUser().getId());
        ShowResult("[OK]");
        return 0;
    }

    /**
     * Показать информацию по задаче
     * @param task задача
     */
    public void viewTask(final Task task) {
        if (task == null) return;
        System.out.println("[VIEW TASK]");
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        ShowResult("[OK]");
    }

    /**
     * Показать задачу по индексу
     * @return 0 выполнено
     */
    public int viewTaskByIndex() throws WrongArgumentException, TaskNotFoundException {
        if (!this.testAuthUser())return 0;
        final int index = EnterIntegerCommandParameter("task index")-1;
        final  Task task = this.getUser().isAdmin() ?
                taskService.findByIndex(index,true):taskService.findByIndex(index,this.getUser().getId(),true);
        viewTask(task);
        return 0;
    }

    /**
     * Показать список задач
     * @param tasks список
     */
    public void viewTasks(List<Task> tasks) {
        if (tasks == null || tasks.isEmpty()) return;
        int index = 1;
        for (final Task task: tasks) {
            System.out.println(index + ". " + task.getId()+ ": " + task.getName());
            index ++;
        }
    }

    /**
     * Показать список задач
     * @return 0 выполнено
     */
    public int listTask() throws WrongArgumentException {
        System.out.println("[LIST TASK]");
        if (!this.testAuthUser())return 0;
        if(this.getUser().isAdmin()) viewTasks(taskService.findAll());
        else viewTasks(taskService.findAll(this.getUser().getId()));
        ShowResult("[OK]");
        return 0;
    }

    /**
     * Показать список задач по идентфиикатору проекта
     * @return 0 выполнено
     */
    public int listTaskByProjectId() throws WrongArgumentException {
        System.out.println("[LIST TASK BY PROJECT ID]");
        if (!this.testAuthUser())return 0;
        final long projectId = EnterLongCommandParameter("project ID");
        if(this.getUser().isAdmin()) viewTasks(taskService.findAllByProjectId(projectId));
        else viewTasks(taskService.findAllByProjectId(projectId,this.getUser().getId()));
        ShowResult("[OK]");
        return 0;
    }

    /**
     * Добавить задачу в проект по идентфиикатору
     * @return 0 выполнео
     */
    public int addTaskToProjectByIds() throws ProjectNotFoundException, TaskNotFoundException, WrongArgumentException {
        final long projectId = EnterLongCommandParameter("project ID");
        final long taskId = EnterLongCommandParameter("task ID");
        projectTaskService.addTaskToProject(projectId,taskId);
        ShowResult("[OK]");
        return 0;
    }

    /**
     * Удалить задачу из проекта по идентификатору
     * @return 0 выполнено
     */
    public int removeTaskFromProjectByIds() throws WrongArgumentException, TaskNotFoundException {
        System.out.println("[REMOVE TASK FROM PROJECT BY ID]");
        if (!this.testAuthUser())return 0;
        final long projectId = EnterLongCommandParameter("project ID");
        final long taskId = EnterLongCommandParameter("task ID");
        if(this.getUser().isAdmin()) projectTaskService.removeTaskFromProject(projectId,taskId);
        else projectTaskService.removeTaskFromProject(projectId,taskId,this.getUser().getId());
        ShowResult("[OK]");
        return 0;
    }

    /**
     * Поменять ИД пользователя в задаче задачу искать по ИД
     * @param user Пользователь на ид которого менять
     * @return 0 выполнено
     */
    public int setTaskUserById(final User user) throws WrongArgumentException, TaskNotFoundException {
        if (!this.testAdminUser())return 0;
        final long taskId = EnterLongCommandParameter("task ID");
        final Task task = taskService.findById(taskId,true);
        taskService.update(task.getId(),task.getName(),task.getDescription(),user.getId());
        ShowResult("[OK]");
        return 0;
    }

    /**
     * Поменять ИД пользователя в задаче задачу искать по индексу
     * @param user Пользователь на ид которого менять
     * @return 0 выполнено
     */
    public int setTaskUserByIndex(final User user) throws WrongArgumentException, TaskNotFoundException {
        if (!this.testAdminUser())return 0;
        final int index = EnterIntegerCommandParameter("task index")-1;
        final Task task = taskService.findByIndex(index,true);
        taskService.update(task.getId(),task.getName(),task.getDescription(),user.getId());
        ShowResult("[OK]");
        return 0;
    }
}
