package com.nlmkit.korshunov_am.tm.controller;

import com.nlmkit.korshunov_am.tm.entity.Command;
import com.nlmkit.korshunov_am.tm.service.CommandHistoryService;

/**
 * Контроллер системных комманд
 */
public class CommandHistoryController extends AbstractController{
    /**
     * Конструтор
     * @param сommandHistoryService сервис системных комманд
     */
    public CommandHistoryController(CommandHistoryService сommandHistoryService) {
        super(сommandHistoryService);
    }
    public int listCommandHistory(){
        System.out.println("[LIST COMMAND HISTORY]");
        int index = 1;
        for (final Command command: this.getСommandHistoryService().findAll())
        {
            System.out.println(index + ". " + command.getCommandText());
            index ++;
        }
        ShowResult("[OK]");
        return 0;
    }

}
