package com.nlmkit.korshunov_am.tm.repository;

import com.nlmkit.korshunov_am.tm.entity.Project;
import com.nlmkit.korshunov_am.tm.entity.Task;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Репозитарий задач
 */
public class TaskRepostory {
    /**
     * Задачи
     */
    private SortedMap<String,List<Task>> tasks = new TreeMap<>();

    /**
     * Создать задачу
     * @param name имя
     * @param userId ид пользователя
     * @return задача
     */
    public Task create(String name,final Long userId) {
        final Task task = new Task(name,userId);
        List<Task> taskList = tasks.get(name);
        if(taskList==null){
            taskList= new ArrayList<>();
            tasks.put(name,taskList);
        }
        taskList.add(task);
        return task;
    }

    /**
     * Создать задачу
     * @param name имя
     * @param description описание
     * @param userId ид пользователя
     * @return задача
     */
    public Task create(final String name,final String description,final Long userId) {
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        task.setUserId(userId);

        List<Task> taskList = tasks.get(name);
        if(taskList==null){
            taskList= new ArrayList<>();
            tasks.put(name,taskList);
        }
        taskList.add(task);
        return task;
    }

    /**
     * Изменить задачу
     * @param id идентификатор
     * @param name имя
     * @param description описание
     * @param userId ид пользователя
     * @return задача
     */
    public Task update(final Long id,final String name,final String description,final Long userId) {
        final Task task = findById(id);
        if (task == null) return null;
        task.setId(id);
        if(!name.equals(task.getName())){
            List<Task> taskListold = tasks.get(task.getName());
            if(taskListold!=null) taskListold.remove(task);
            List<Task> taskListNew = tasks.get(name);
            if(taskListNew==null) {
                taskListNew= new ArrayList<>();
                tasks.put(name,taskListNew);
            }
            taskListNew.add(task);
        }
        task.setName(name);
        task.setDescription(description);
        task.setUserId(userId);
        return task;
    }

    /**
     * Удалить все задачи.
     */
    public void clear() {
        tasks.clear();
    }

    /**
     * Удалить все задачи пользователя.
     * @param userId ид пользователя
     */
    public void clear(final Long userId) {
        tasks.forEach((taskName,taskList) -> {
            taskList.removeIf(task -> task.getUserId().equals(userId));
        } );
    }

    /**
     * Найти задачу по индексу.
     * @param index Индекс
     * @return задача
     */
    public Task findByIndex(final int index){
        int currentIndex = 0;
        for (Map.Entry<String, List<Task>> entry : tasks.entrySet()) {
            List<Task> taskList = entry.getValue();
            if (index >= currentIndex + taskList.size())
                currentIndex += taskList.size();
            else
                return taskList.get(index-currentIndex);
        }
        return null;
    }

    /**
     * Найти задачу по индексу и пользователю.
     * @param index Индекс
     * @param userId ид пользователя
     * @return задача
     */
    public Task findByIndex(final int index,final Long userId){
        if (userId==null) return  null;
        Task task = findByIndex(index);
        if (task == null) return null;
        if (!task.getUserId().equals(userId)) return null;
        return task;
    }

    /**
     * Найти задачу по имени.
     * @param name имя
     * @return задача
     */
    public Task findByName(final String name){
        List<Task> taskList = tasks.get(name);
        if (taskList.size()>0) return taskList.get(0);
        return null;
    }

    /**
     * Найти задачу по имени и пользователю.
     * @param userId ид пользователя
     * @param name имя
     * @return задача
     */
    public Task findByName(final String name,final Long userId){
        if (userId==null) return  null;
        List<Task> taskList = tasks.get(name);
        for (final Task task: taskList) {
            if (task.getName().equals(name) && task.getUserId().equals(userId)) return task;
        }
        return null;
    }

    /**
     * Найти задачу по идентификатору.
     * @param id идентификатор
     * @return задача
     */
    public Task findById(final Long id){
        for (Map.Entry<String, List<Task>> entry : tasks.entrySet()) {
            List<Task> taskList = entry.getValue();
            for (final Task task: taskList)
                if (task.getId().equals(id)) return task;
        }
        return null;
    }

    /**
     * Найти задачу по идентификатору и пользователю.
     * @param id идентификатор
     * @param userId ид пользователя
     * @return задача
     */
    public Task findById(final Long id,final Long userId){
        if (userId==null) return  null;
        final Task task = findById(id);
        if (task.getUserId().equals(userId)) return task;
        return null;
    }

    /**
     * Ищем задачу по идентификатору проекта и по идентификатору задачи
     * @param projectId идентификатор проекта
     * @param id идентификатор задачи
     * @return задача
     */
    public Task findByProjectIdAndId(final Long projectId,final Long id){
        final Task task = findById(id);
        if (task == null) return null;
        final Long idProject = task.getProjectId();
        if (idProject == null) return null;
        if (!idProject.equals(projectId))  return null;
        return task;
    }
    /**
     * Ищем задачу по идентификатору проекта идентификатору задачи и пользователю.
     * @param projectId идентификатор проекта
     * @param id идентификатор задачи
     * @param userId ид пользователя
     * @return задача
     */
    public Task findByProjectIdAndId(final Long projectId,final Long id,final Long userId){
        final Task task= findByProjectIdAndId(projectId,id);
        if (task == null) return null;
        if (task.getUserId().equals(userId)) return task;
        return null;
    }
    /**
     * Удалить задачу по индексу
     * @param index индекс
     * @return задача
     */
    public Task removeByIndex(final int index){
        final Task task = findByIndex(index);
        if (task == null) return null;
        List<Task> taskList = tasks.get(task.getName());
        taskList.remove(task);
        return task;
    }

    /**
     * Удалить задачу по индексу и пользователю.
     * @param index индекс
     * @param userId ид пользователя
     * @return задача
     */
    public Task removeByIndex(final int index,final Long userId){
        final Task task = findByIndex(index,userId);
        if (task == null) return null;
        List<Task> taskList = tasks.get(task.getName());
        taskList.remove(task);
        return task;
    }
    /**
     * Удалить задачу по идентификатору
     * @param id идентификатор
     * @return задача
     */
    public Task removeById(final Long id){
        final Task task = findById(id);
        if (task == null) return null;
        List<Task> taskList = tasks.get(task.getName());
        taskList.remove(task);
        return task;
    }

    /**
     * Удалить задачу по идентификатору и пользователю.
     * @param id идентификатор
     * @param userId ид пользователя
     * @return задача
     */
    public Task removeById(final Long id,final Long userId){
        final Task task = findById(id,userId);
        if (task == null) return null;
        List<Task> taskList = tasks.get(task.getName());
        taskList.remove(task);
        return task;
    }

    /**
     * Удалить задачу по имени
     * @param name имя
     * @return задча
     */
    public Task removeByName(final String name){
        final Task task = findByName(name);
        if (task == null) return null;
        List<Task> taskList = tasks.get(task.getName());
        taskList.remove(task);
        return task;
    }

    /**
     * Удалить задачу по имении пользователю и пользователю.
     * @param name имя
     * @param userId ид пользователя
     * @return задча
     */
    public Task removeByName(final String name,final Long userId){
        final Task task = findByName(name,userId);
        if (task == null) return null;
        List<Task> taskList = tasks.get(task.getName());
        taskList.remove(task);
        return task;
    }

    /**
     * Найти по идентификатору проект и добавить к нему задачу
     * @param projectId идентификатор проекта
     * @param taskId идентификатор задачи
     * @return задача null если задачи не найдено
     */
    public Task findAddByProjectId(final Long projectId,final Long taskId) {
        final Task task = findByProjectIdAndId(projectId,taskId);
        if (task == null) return null;
        task.setProjectId(projectId);
        return task;
    }

    /**
     * Найти по идентификатору проект и добавить к нему задачу
     * задача должны быть указанного пользователя
     * @param projectId идентификатор проекта
     * @param taskId идентификатор задачи
     * @param userId ид пользователя
     * @return задача null если задачи не найдено
     */
    public Task findAddByProjectId(final Long projectId,final Long taskId,final Long userId) {
        Task task = findById(taskId,userId);
        if(task==null) return null;
        task.setProjectId(projectId);
        return task;
    }

    /**
     * Получить список задач проекта
     * @param projectId идентрификатор проектв
     * @return списрк задач
     */
    public List<Task> findAllByProjectId(final Long projectId) {
        final List<Task> result = new ArrayList<>();
        for (final Task task: findAll()) {
            final Long idProject = task.getProjectId();
            if (idProject == null) continue;
            if (projectId.equals(idProject)) result.add(task);
        }
        return result;
    }

    /**
     * Получить список задач проекта с учетом пользователя
     * @param projectId идентрификатор проектв
     * @param userId ид пользователя
     * @return списрк задач
     */
    public List<Task> findAllByProjectId(final Long projectId,final Long userId) {
        final List<Task> result = new ArrayList<>();
        for (Map.Entry<String, List<Task>> entry : tasks.entrySet()) {
            List<Task> taskList = entry.getValue();
            for (final Task task: taskList){
                final Long idProject = task.getProjectId();
                if (idProject == null) continue;
                if (!task.getUserId().equals(userId))continue;
                if (projectId.equals(idProject)) result.add(task);
            }
        }
        return result;
    }
    /**
     * Получить список всех задач
     * @return список задач
     */
    public List<Task> findAll() {
        final List<Task> result = new ArrayList<>();
        for (Map.Entry<String, List<Task>> entry : tasks.entrySet()) {
            List<Task> taskList = entry.getValue();
            for (final Task task: taskList){
                result.add(task);
            }
        }
        return result;
    }

    /**
     * Получить список всех задач пользователя
     * @param userId ид пользователя
     * @return список задач
     */
    public List<Task> findAll(final Long userId) {
        final List<Task> result = new ArrayList<>();
        for (Map.Entry<String, List<Task>> entry : tasks.entrySet()) {
            List<Task> taskList = entry.getValue();
            for (final Task task: taskList){
                if(task.getUserId().equals(userId)) result.add(task);
            }
        }
        return result;
    }

    /**
     * Получить количество задач
     * @return количество задач
     */
    public int size() {
        Integer result=0;
        for (Map.Entry<String, List<Task>> entry : tasks.entrySet()) {
            List<Task> taskList = entry.getValue();
            result+=taskList.size();
        }
        return result;
    }

    /**
     * Получить количество задач пользователя
     * @param userId ид пользователя
     * @return количество задач
     */
    public int size(final Long userId) {
        Integer result=0;
        for (Map.Entry<String, List<Task>> entry : tasks.entrySet()) {
            List<Task> taskList = entry.getValue();
            for (final Task task: taskList)result++;
        }
        return result;
    }
}
